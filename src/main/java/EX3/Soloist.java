package EX3;

public interface Soloist {
    public void playLikeASoprano();

    public void playLikeATenor();

}
