package EX3;

public class Flute extends Woodwind implements Soloist {

    public void playLikeASoprano() {
        System.out.println("Flute Plays like a Soprano ");
    }

    public void playLikeATenor() {
        System.out.println("Flute Plays like a Tenor ");
    }

    public void play(){
        System.out.println("Flute Plays like an instrument");
    }
}

