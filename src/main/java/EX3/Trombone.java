package EX3;

public class Trombone extends Brass implements Soloist{

    public void playLikeASoprano() {
        System.out.println("Trombone Plays like a Soprano");
    }

    public void playLikeATenor(){
        System.out.println("Trombone Plays like a Tenor");
    }

    public void play(){
        System.out.println("Trombone plays like an instrument");
    }
}
