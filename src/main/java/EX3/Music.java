package EX3;

public class Music {
    public static void main(String[] args){
Orchestra o = new Orchestra();
        Percussion p = new Percussion();
        o.addInstrument(p);
        Trombone t = new Trombone();
        o.addInstrument(t);
        Guitar g = new Guitar();
        o.addInstrument(g);
        Violin v = new Violin();
        o.addInstrument(v);
        Flute f = new Flute();
        o.addInstrument(f);

        o.tuneAll();
        o.tuneTenor(t);
        o.tuneSoprano(v);
        o.tuneSoprano(f);
        o.tuneTenor(g);
    }
}
