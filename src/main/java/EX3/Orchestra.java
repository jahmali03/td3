package EX3;

import java.util.ArrayList;
import java.util.List;

public class Orchestra {
    private List<Instrument> instrumentList = new ArrayList<>();

    public void tune(Instrument instrument){
        instrument.play();
    }

    public void tuneAll(){
        for(Instrument i :instrumentList){
            tune(i);
        }
    }

    public void addInstrument(Instrument instrument){
        instrumentList.add(instrument);
    }

    public void tuneTenor(Soloist i){
        i.playLikeATenor();
    }

    public void tuneSoprano(Soloist i){
        i.playLikeASoprano();
    }
}

