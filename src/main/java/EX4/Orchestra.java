package EX4;

import EX3.Soloist;

import java.util.ArrayList;
import java.util.List;

public class Orchestra {
    private List<Instrument> instrumentList = new ArrayList<>();

    public void tune(Instrument instrument){
        instrument.play();
    }

    public void tuneAll(){
        for(Instrument i :instrumentList){
            tune(i);
        }
    }

    public void addInstrument(Instrument instrument){
        instrumentList.add(instrument);
    }

    public void tuneTenor(Tenor t){
        t.playLikeATenor();
    }

    public void tuneSoprano(Soprano s){
        s.playLikeASoprano();
    }
}

