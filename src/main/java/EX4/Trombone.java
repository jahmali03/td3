package EX4;

public class Trombone extends Brass implements Tenor {


    public void playLikeATenor(){
        System.out.println("Trombone Plays like a Tenor");
    }

    public void play(){
        System.out.println("Trombone plays like an instrument");
    }
}
