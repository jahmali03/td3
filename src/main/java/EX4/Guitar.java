package EX4;

import EX3.Soloist;

public class Guitar extends Stringed implements Tenor {

    public void playLikeASoprano() {
        System.out.println("Guitar Plays Like a Soprano");
    }

    public void playLikeATenor() {
        System.out.println("Guitar Plays like a Tenor ");
    }

    public void play(){
        System.out.println("Guitar Plays like an instrument");
    }
}
