package EX4;

public class Violin extends Stringed implements Soprano {


    public void playLikeASoprano() {
        System.out.println("Violin Plays like a Soprano ");
    }

    public void playLikeATenor() {
        System.out.println("Violin Plays like a Tenor ");
    }

    public void play(){
        System.out.println("Violin Plays like an instrument");
    }
}
