package EX4;

import EX3.Soloist;

public class Flute extends Woodwind implements Soprano {

    public void playLikeASoprano() {
        System.out.println("Flute Plays like a Soprano ");
    }

    public void play(){
        System.out.println("Flute Plays like an instrument");
    }
}

