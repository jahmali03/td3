package EX1;

import java.util.ArrayList;
import java.util.List;

public class Orchestra {
    private List<Wind> windList = new ArrayList<>();
    private List<Percussion> percussionList = new ArrayList<>();

    public void tuneWind(Wind windtune){

        windtune.play();
    }

    public void tunePercussion(Percussion percussiontune){

        percussiontune.play();
    }

    public void tuneAll(){
        for(Wind w : windList){
            tuneWind(w);
        }

        for(Percussion p : percussionList){
            tunePercussion(p);
        }
    }

    public void addWind(Wind wind){
        windList.add(wind);
    }

    public void addPercussion(Percussion percussion){
        percussionList.add(percussion);
    }
}
