package EX1;

public class Music {
    public static void main(String[] args){
        Orchestra o = new Orchestra();
        Brass b = new Brass();
        o.addWind(b);
        Woodwind w = new Woodwind();
        o.addWind(w);
        Wind w2 = new Wind();
        o.addWind(w2);
        Percussion p = new Percussion();
        o.addPercussion(p);

        o.tuneAll();

    }
}
