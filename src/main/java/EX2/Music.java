package EX2;

public class Music {
    public static void main(String[] args){
Orchestra o = new Orchestra();
        Brass b = new Brass();
        o.addInstrument(b);
        Woodwind w = new Woodwind();
        o.addInstrument(w);
        Wind w2 = new Wind();
        o.addInstrument(w2);
        Percussion p = new Percussion();
        o.addInstrument(p);
        Stringed s = new Stringed();
        o.addInstrument(s);

        o.tuneAll();
    }
}
