Exercise 1:

1.2
a)
It is hard to add new types of instruments since we have to add new lines of code in the 
orchestra class and in music.java and we have to create a new class

b)
it's also hard to reuse the orchestra to compose a new music because we must delete all instruments in each list and recreate as many instruments we need. it can be really long according to the number of instruments we need to add.

1.3

This design does not follow the SOLID open/closed principle since we have to change many entities to play new music instead of juste adding a new class extending an existing one.

1.4

If i were to put in pause all the brass instruments i would have to delete all the brass instruments which is not really appropriate. the Liskov substitution principle is not respected at all

1.5

the dependency inversion principle is not respected neither since any modification in a instrument class will affetct all bigger classes.


Exercise 2:

2.3

a)
Now we can add new classes of instruments without modifying other classes 

b)
with this design we don't need to change the orchestra class to compose a new music, we just need to add or remove instruments from the instruments list.

2.4

the Open/closed principle is respected since we can add new classes of instruments without modifying any existing class.

2.5

To put all the brass instruments in pause, since we can tune instruments one by one, we must tune all the instruments unless the brass ones. No deletion and no code modification.

2.6

Now, the dependency inversion principle is respected as high level entities does not depend on low level entities anymore.

